interface Iface
{
    public void bark();
    public void walk();
}
class Main implements Iface
{
    public void bark()
    {
        System.out.println("Sound.....\n");
    }
    public void walk()
    {
        System.out.println("Walk.....");
    }
    public static void main(String[] args)
    {
        Main a = new Main();
        a.walk();
        a.bark();
    }
}