class Base
{
	public int base(int i)
	{
		System.out.println("Base Output : ");
		return i*3;
	}
}
class Derived extends Base
{
	public double derived(double i)
	{
		System.out.println("Derived Output : ");
		return i + 4.9;
	}
}
class Main
{
	public static void main(String args[])
	{
		Derived obj = new Derived();
		System.out.println(obj.base(9));
		System.out.println(obj.derived(4.50));
	}
}