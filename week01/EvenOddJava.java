
import java.util.*;
class Main
{
    void evenodd(int a)
    {
        if(a%2==0)
        {
            System.out.println("number "+a+" is Even");
        }
        else
        {
            System.out.println("number "+a+" is Odd");
        }
    }
    public static void main (String[] args) {
        int a;
        Main obj = new Main();
        Scanner input =new Scanner(System.in);
        System.out.println("Enter a number : ");
        a = input.nextInt();
        obj.evenodd(a);
    }}
