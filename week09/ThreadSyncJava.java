class Table{  
 synchronized void printTable(int n){//synchronized method  
   for(int i=1;i<=5;i++){  
     System.out.println(n*i);  
     try{  
      Thread.sleep(400);  
     }catch(Exception e){System.out.println(e);}  
   }  
  
 }  
}  
  
public class Main{  
public static void main(String args[]){  
final Table obj = new Table();
Thread t1=new Thread(){  
public void run(){  
obj.printTable(9);  
}  
};  
Thread t2=new Thread(){  
public void run(){  
obj.printTable(17);  
}  
};  
t1.start();  
t2.start();  
}  
} 